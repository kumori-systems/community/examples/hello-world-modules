package kmodule

{
	domain: "kumori.examples"
	module: "hello-world-modules"
	cue:    "0.4.2"
	version: [
		0,
		0,
		3,
	]
	dependencies: {
		"kumori.systems/kumori": {
			target: "kumori.systems/kumori/@1.1.6"
			query:  "1.1.6"
		}
		"kumori.systems/builtins/inbound": {
			target: "kumori.systems/builtins/inbound/@1.3.0"
			query:  "1.3.0"
		}
	}
	sums: {
		"kumori.systems/kumori/@1.1.6":           "jsXEYdYtlen2UgwDYbUCGWULqQIigC6HmkexXkyp/Mo="
		"kumori.systems/builtins/inbound/@1.3.0": "F3nipPPUCZ4YpsAh+Xnh9t8W1Tu98eX6SHRVM3BbRYs="
	}
	spec: [
		1,
		0,
	]
}
