package service

import (
  k "kumori.systems/kumori:kumori"
  f ".../frontend:component"
  i "kumori.systems/builtins/inbound:service"
)

// In Kumori Platform, a Service Application represents a set of interconnected
// Kumori Components working together.
// This 'Artifact' describes the Kumori Service Application.
#Artifact: {
  ref: name:  "service"

  description: {

    //
    // Kumori Component roles and configuration
    //

    // Configuration (parameters and resources) to be provided to the Kumori
    // Service Application.
    config: {
      parameter: {}
      resource: {
        servercert: k.#Certificate
        serverdomain: k.#Domain
      }
    }

    // List of Kumori Components of the Kumori Service Application.
    // In a service, the same component could be run playing multiple roles, with
    // different purposes, each of them with its own configuration.
    // In this case there is one role ("frontend") for only one component
    // described in the imported component manifest, and another role "httpinbound"
    // for the HTTP-Inbound subservice
    role: {
      frontend: {
        artifact: f.#Artifact
        // Configuration spread: computing each role configuration based on service
        // configuration (no configuration in this example).
        config: {
          parameter: {}
          resource: {}
          resilience: description.config.resilience
        }
      }
      httpinbound: {
        artifact: i.#Artifact
        config: {
          parameter: {
            type: "https"
            websocket: false
          }
          resource: {
            servercert: description.config.resource.servercert
            serverdomain: description.config.resource.serverdomain
          }
          resilience: description.config.resilience
        }
      }
    }

    //
    // Kumori Service topology: how roles are interconnected
    //

    srv: {} // No service channels: the inbound is included as part of the service

    // Connectors specify the topology graph.
    // In this case, there is only one load-balancer connector connecting the
    // inbound and frontend channels
    connect: {
      // inbound => frontend
      cinbound: {
        as: "lb"
        from: httpinbound: "inbound"
        to: frontend: restapi: _
      }
    }
  }
}
