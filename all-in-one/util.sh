#!/bin/bash

# Cluster variables
CLUSTERNAME="demo"
REFERENCEDOMAIN="test.kumori.cloud"
CLUSTERCERT="cluster.core/wildcard-test-kumori-cloud"

# Service variables
DEPLOYNAME="helloworlddep"
DOMAIN="helloworlddomain"
SERVICEURL="helloworld-${CLUSTERNAME}.${REFERENCEDOMAIN}"

KUMORI_SERVICE_MODEL_VERSION="1.1.6"
KUMORI_INBOUND_VERSION="1.3.0"

case $1 in

'refresh-dependencies')
  # Dependencies are removed and added again just to ensure that the right versions
  # are used.
  # This is not necessary, if the versions do not change!
  kam mod dependency --delete kumori.systems/kumori
  kam mod dependency --delete kumori.systems/builtins/inbound
  kam mod dependency kumori.systems/kumori/@${KUMORI_SERVICE_MODEL_VERSION}
  kam mod dependency kumori.systems/builtins/inbound/@${KUMORI_INBOUND_VERSION}
  ;;

'create-domain')
  kam ctl register domain $DOMAIN --domain $SERVICEURL
  ;;

'dry-run')
  kam process deployment -t .
  ;;

'deploy-service')
  kam service deploy $DEPLOYNAME -d deployment -t .  -- --comment "Hello world service" --wait 5m
  ;;

'update-service')
  kam service update $DEPLOYNAME -d deployment -t .  -- --comment "Updating" --wait 5m
  ;;

'describe')
  kam service describe $DEPLOYNAME
  ;;

'describe-json')
  kam service describe $DEPLOYNAME -- -ojson
  ;;

'test')
  curl https://${SERVICEURL}/hello
  ;;

'undeploy-service')
  kam service undeploy $DEPLOYNAME -- --wait 5m
  ;;

'delete-domain')
  kam ctl unregister domain $DOMAIN
  ;;

*)
  echo "This script doesn't contain that command"
	;;

esac
