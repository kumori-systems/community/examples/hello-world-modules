package kmodule

{
	domain: "kumori.examples"
	module: "helloworld-service"
	cue:    "0.4.2"
	version: [
		0,
		0,
		2,
	]
	dependencies: {
		"kumori.systems/builtins/inbound": {
			target: "kumori.systems/builtins/inbound/@1.3.0"
			query:  "1.3.0"
		}
		"kumori.examples/helloworld-components": {
			target: "kumori.examples/helloworld-components/@0.0.1"
			query:  "../helloworld-components"
		}
	}
	sums: {
		"kumori.systems/builtins/inbound/@1.3.0":       "F3nipPPUCZ4YpsAh+Xnh9t8W1Tu98eX6SHRVM3BbRYs="
		"kumori.examples/helloworld-components/@0.0.1": "qLtMHZ8Tgq+/6CC/0eRLC7QL+5OhiqMAPjSbqe3q9yI="
	}
	spec: [
		1,
		0,
	]
}
