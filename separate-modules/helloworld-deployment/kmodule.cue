package kmodule

{
	domain: "kumori.examples"
	module: "helloworld-deployment"
	cue:    "0.4.2"
	version: [
		0,
		0,
		1,
	]
	dependencies: {
		"kumori.systems/kumori": {
			target: "kumori.systems/kumori/@1.1.6"
			query:  "1.1.6"
		}
		"kumori.examples/helloworld-service": {
			target: "kumori.examples/helloworld-service/@0.0.2"
			query:  "../helloworld-service"
		}
	}
	sums: {
		"kumori.systems/kumori/@1.1.6":              "jsXEYdYtlen2UgwDYbUCGWULqQIigC6HmkexXkyp/Mo="
		"kumori.examples/helloworld-service/@0.0.2": "DKiIxq4RgRhh92EHlsYMefqwHLa8nibHlMNGjq77q0I="
	}
	spec: [
		1,
		0,
	]
}
