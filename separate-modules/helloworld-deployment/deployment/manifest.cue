package deployment

import (
  s "kumori.examples/helloworld-service/service:service"
)

// A Kumori Service Application is deployed as a service within a Kumori Platform.
// '#Deployment' describes the configuration to be used.
#Deployment: {
  name: "helloworlddep"

  // Imported service aplication manifest to be deployed
  artifact: s.#Artifact

  config: {

    // Configuration (parameters and resources) to be injected.
    parameter: {}
    resource: {
      servercert: {
        // Builtin TLS certificate resource related to the platform reference domain
        certificate: "cluster.core/wildcard-test-kumori-cloud"
      }
      serverdomain:  {
        // Domain resource used to access the service.
        domain: "helloworlddomain"
      }
    }

    // Horizontal size: in this case, just the number of instances of the
    // frontend role, and service resilience to failures.
    // Httpinbound is an special role (inbound builtin service), so it is
    // not required to define its size
    scale: detail: {
      frontend: hsize: 2
    }
    resilience: 1
  }
}
