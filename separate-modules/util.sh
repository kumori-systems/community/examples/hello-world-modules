#!/bin/bash

# Cluster variables
CLUSTERNAME="demo"
REFERENCEDOMAIN="test.kumori.cloud"
CLUSTERCERT="cluster.core/wildcard-test-kumori-cloud"

# Service variables
DEPLOYNAME="helloworlddep"
DOMAIN="helloworlddomain"
SERVICEURL="helloworld-${CLUSTERNAME}.${REFERENCEDOMAIN}"

KUMORI_SERVICE_MODEL_VERSION="1.1.6"
KUMORI_INBOUND_VERSION="1.3.0"

case $1 in

'refresh-dependencies-local')
  cd helloworld-components
  kam mod dependency --delete kumori.systems/kumori
  kam mod dependency kumori.systems/kumori/@${KUMORI_SERVICE_MODEL_VERSION}
  cd ..
  cd helloworld-service
  kam mod dependency --delete kumori.systems/builtins/inbound
  kam mod dependency --delete kumori.examples/helloworld-components
  kam mod dependency kumori.systems/builtins/inbound/@${KUMORI_INBOUND_VERSION}
  kam mod dependency ../helloworld-components kumori.examples/helloworld-components
  cd ..
  cd helloworld-deployment
  kam mod dependency --delete kumori.systems/kumori
  kam mod dependency --delete kumori.examples/helloworld-service
  kam mod dependency kumori.systems/kumori/@${KUMORI_SERVICE_MODEL_VERSION}
  kam mod dependency ../helloworld-service kumori.examples/helloworld-service
  cd ..
  ;;

'refresh-dependencies-npm')
  cd helloworld-components
  kam mod dependency --delete kumori.systems/kumori
  kam mod dependency kumori.systems/kumori/@${KUMORI_SERVICE_MODEL_VERSION}
  kam mod publish
  cd ..
  cd helloworld-service
  kam mod dependency --delete kumori.systems/builtins/inbound
  kam mod dependency --delete kumori.examples/helloworld-components
  kam mod dependency kumori.systems/builtins/inbound/@${KUMORI_INBOUND_VERSION}
  kam mod dependency kumori.examples/helloworld-components/@0.0.1
  kam mod publish
  cd ..
  cd helloworld-deployment
  kam mod dependency --delete kumori.systems/kumori
  kam mod dependency --delete kumori.examples/helloworld-service
  kam mod dependency kumori.systems/kumori/@${KUMORI_SERVICE_MODEL_VERSION}
  kam mod dependency kumori.examples/helloworld-service/@0.0.2
  cd ..
  ;;

'create-domain')
  kam ctl register domain $DOMAIN --domain $SERVICEURL
  ;;

'dry-run')
  kam process deployment -t ./helloworld-deployment
  ;;

'deploy-service')
  kam service deploy $DEPLOYNAME -d deployment -t ./helloworld-deployment -- --comment "Hello world service" --wait 5m
  ;;

'update-service')
  kam service update $DEPLOYNAME -d deployment -t ./helloworld-deployment -- --comment "Updating" --wait 5m
  ;;

'describe')
  kam service describe $DEPLOYNAME
  ;;

'describe-json')
  kam service describe $DEPLOYNAME -- -ojson
  ;;

'test')
  curl https://${SERVICEURL}/hello
  ;;

'undeploy-service')
  kam service undeploy $DEPLOYNAME -- --wait 5m
  ;;

'delete-domain')
  kam ctl unregister domain $DOMAIN
  ;;

*)
  echo "This script doesn't contain that command"
	;;

esac
